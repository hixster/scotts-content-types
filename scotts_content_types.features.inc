<?php
/**
 * @file
 * scotts_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function scotts_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_custom_pub_defaults().
 */
function scotts_content_types_custom_pub_defaults() {
  $options = array();
  // Exported option: show_in_listings
  $options['show_in_listings'] = array(
    'type' => 'show_in_listings',
    'name' => t('Show in listings'),
    'node_types' => array(
      'article' => t('Article'),
      'product' => t('Product'),
      'trade_product' => t('Trade product'),
    ),
  );

  return $options;
}

/**
 * Implements hook_image_default_styles().
 */
function scotts_content_types_image_default_styles() {
  $styles = array();

  // Exported image style: article_main.
  $styles['article_main'] = array(
    'name' => 'article_main',
    'effects' => array(),
  );

  // Exported image style: article_teaser.
  $styles['article_teaser'] = array(
    'name' => 'article_teaser',
    'effects' => array(),
  );

  // Exported image style: product_main.
  $styles['product_main'] = array(
    'name' => 'product_main',
    'effects' => array(
      1 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '317',
          'height' => '208',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: product_teaser.
  $styles['product_teaser'] = array(
    'name' => 'product_teaser',
    'effects' => array(),
  );

  // Exported image style: trade_product_main.
  $styles['trade_product_main'] = array(
    'name' => 'trade_product_main',
    'effects' => array(),
  );

  // Exported image style: trade_product_teaser.
  $styles['trade_product_teaser'] = array(
    'name' => 'trade_product_teaser',
    'effects' => array(),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function scotts_content_types_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('A content type for adding products the site. Amazon store items can be added using this content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'trade_product' => array(
      'name' => t('Trade product'),
      'base' => 'node_content',
      'description' => t('A product which can be used in the trade area. It allows for the uploading of marketing materials and data sheets'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
